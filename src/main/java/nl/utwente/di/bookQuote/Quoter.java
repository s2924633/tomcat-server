package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private final Map<String,Double> values = Map.of("1", 10.0, "2", 45.0, "3", 20.0, "4", 35.0, "5", 50.0, "others", 0.0);
    public double getBookPrice(String isbn) {
        return values.get(isbn);
    }
}
